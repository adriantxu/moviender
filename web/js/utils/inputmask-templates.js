/**
 * Agrega la mascara a los campos con el atributo data especifico
 * 
 * @returns {undefined}
 */
function initInputMask() {
    $('[data-dni-inputmask]').inputmask('99999999-A');

    $('[data-phone-inputmask]').inputmask('999 999 999');

    $('[data-bank-account-inputmask]').inputmask('AA99 9999 9999 99 9999999999');

    $('[data-money-inputmask]').inputmask({alias: 'numeric', 'allowMinus': false, 'groupSeparator': '.', 'autoGroup': true, 'digits': 2, 'digitsOptional': false});
}
;

/**
 * Comprueba que los campos con mascara esten bien rellenados
 * 
 * @returns {undefined}
 */
function checkInputMask() {
    $('.sonata-ba-form > form').on('submit', function (e) {
        var wayToGo = true;
        var fieldMaskError = '';

        $('[data-dni-inputmask][required="required"]').each(function () {
            var complete = $(this).inputmask('isComplete');

            if (!complete) {
                fieldMaskError = $(this).attr('id');

                wayToGo = false;

                return false;
            }
        });

        $('[data-phone-inputmask][required="required"]').each(function () {
            var complete = $(this).inputmask('isComplete');

            if (!complete) {
                fieldMaskError = $(this).attr('id');

                wayToGo = false;

                return false;
            }
        });

        if (wayToGo) {
            $('[data-bank-account-inputmask][required="required"]').each(function () {
                var complete = $(this).inputmask('isComplete');

                if (!complete) {
                    fieldMaskError = $(this).attr('id');

                    wayToGo = false;

                    return false;
                }
            });
        }

        if (wayToGo) {
            $('[data-money-inputmask][required="required"]').each(function () {
                var complete = $(this).inputmask('isComplete');

                if (!complete) {
                    fieldMaskError = $(this).attr('id');

                    wayToGo = false;

                    return false;
                }
            });
        }

        // Eliminamos las clases de errores
        $(this).find('.has-error').removeClass('has-error');

        if (!wayToGo) {
            e.preventDefault();
            e.stopPropagation();

            var tabIndex = $('#' + fieldMaskError).closest('.tab-pane').index();

            $('#' + fieldMaskError).closest('.nav-tabs-custom').find('.nav.nav-tabs li').eq(tabIndex).find('a').trigger('click');

            $('#' + fieldMaskError).closest('.nav-tabs-custom').find('.nav.nav-tabs li').eq(tabIndex).find('i').removeClass('hide');
            $('#' + fieldMaskError).closest('.nav-tabs-custom').find('.nav.nav-tabs li').eq(tabIndex).find('i').addClass('has-errors');

            $('#' + fieldMaskError).closest('.form-group').addClass('has-error');

            alert('Campo incompleto');

            $('#' + fieldMaskError).focus();
        }

    });
}
;

$(document).ready(function () {
    initInputMask();

    checkInputMask();
});