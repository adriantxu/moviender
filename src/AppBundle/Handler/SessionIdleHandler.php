<?php

namespace AppBundle\Handler;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SessionIdleHandler {

    private $container;
    private $session;
    private $securityToken;
    private $router;
    private $maxIdleTime;

    public function __construct($container, SessionInterface $session, TokenStorageInterface $securityToken, RouterInterface $router, $maxIdleTime = 0) {
        $this->container = $container;
        $this->session = $session;
        $this->securityToken = $securityToken;
        $this->router = $router;
        $this->maxIdleTime = $maxIdleTime;
    }

    public function onKernelRequest(GetResponseEvent $event) {
        if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        try {
            $controller = $event->getRequest()->get('_controller');
            $controllerExploded = explode('\\', $controller);

            // El ApiBundle no necesita comprobars
            if (in_array(strtolower($controllerExploded[0]), ['apibundle'])) {
                return;
            }
        } catch (\Exception $e) {
            
        }

        // Si el usuario es anonimo no hay por que desloguear al usuario
        $token = $this->container->get('security.token_storage')->getToken();

        $userIsLogged = false;

        if ($token && $user = $token->getUser()) {
            if ($user instanceof \Application\Sonata\UserBundle\Entity\User) {
                $user = true;
            }
        }

        if ($userIsLogged && $this->maxIdleTime > 0 && !isset($_COOKIE['REMEMBERME'])) {
            @$this->session->start();
            $lapse = time() - $this->session->getMetadataBag()->getLastUsed();

            if ($lapse > $this->maxIdleTime) {
                $this->securityToken->setToken(null);
                $this->session->getFlashBag()->set('info', $this->container->get('translator')->trans('inactivity_logout', [], 'SonataAdminBundle'));

                $event->setResponse(new RedirectResponse($this->router->generate('sonata_user_admin_security_login')));
            }
        }
    }

}
