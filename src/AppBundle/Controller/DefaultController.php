<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        return $this->redirectToRoute('sonata_admin_dashboard');
    }

    /**
     * Actualiza el locale de la aplicacion
     * 
     * @Route("set_locale", name="set_locale", methods={"POST"})
     */
    public function setLocale(Request $request) {
        $baseResponse = [
            'success' => false,
            'message' => 'UNAUTHORIZED'
        ];

        $baseStatus = 401;

        if ($request->isXmlHttpRequest()) {
            try {
                $translator = \AppBundle\DependencyInjection\UtilsExtension::getTranslator('Api');

                $lang = $request->request->get('lang');

                if (!$lang) {
                    throw new \Exception($translator->trans('errors.mandatory_param', ['%PARAMETRO%' => 'lang']));
                }

                if (!in_array($lang, array_flip(\AppBundle\DependencyInjection\UtilsExtension::getLanguages()))) {
                    $translator = \AppBundle\DependencyInjection\UtilsExtension::getTranslator('SonataAdminBundle', true, 'xliff');

                    throw new \Exception($translator->trans('errors.language_not_found'));
                }

                $request->getSession()->set('_locale', $lang);

                $baseResponse = [
                    'success' => true,
                    'message' => 'OK'
                ];

                $baseStatus = 100;
            } catch (\Exception $e) {
                $baseResponse = [
                    'success' => false,
                    'message' => 'Server error'
                ];

                $baseStatus = 500;
            }
        }

        return new \Symfony\Component\HttpFoundation\ JsonResponse($baseResponse, $baseStatus);
    }

}
