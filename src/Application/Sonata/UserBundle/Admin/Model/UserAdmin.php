<?php

declare(strict_types = 1);

namespace Application\Sonata\UserBundle\Admin\Model;

use Application\Sonata\UserBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends AbstractAdmin {

    /**
     * Si el usuario logueado no es SUPER_ADMIN no se muestran los usuarios SUPER_ADMIN
     * 
     * @param string $context
     * 
     * @return \Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery
     */
    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        if (!$this->getCurrentUser()->isSuperAdmin()) {
            $query
                    ->leftJoin($query->getRootAliases()[0] . '.groups', 'g')
                    ->andWhere(
                            $query->expr()->orX(
                                    $query->expr()->notLike('g.roles', '?1')
                                    , $query->expr()->isNull('g.roles')
                            )
                    )->setParameter(1, '%' . \Sonata\UserBundle\Model\UserInterface::ROLE_SUPER_ADMIN . '%')
            ;
        }

        return $query;
    }

    /**
     * Hay que comprobar si el usuario se esta editando/viendo/eliminando a si mismo
     * 
     * @param string|array                               $name
     * @param \Application\Sonata\UserBundle\Entity\User $object
     * 
     * @return boolean
     */
    public function isGranted($name, $object = null) {
        $isGranted = parent::isGranted($name, $object);

        $name = (is_array($name)) ? $name[0] : $name;

        $user = $this->getCurrentUser();

        if (!($user->isSuperAdmin())) {
            // Si el usuario logueado no es SUPER_ADMIN y el objeto que quiere editar si lo es, no le damos permiso
            if ($object && $object->isSuperAdmin()) {
                $isGranted = false;
            }

            // Si el usuario logueado es ROLE_USER y puede entrar a la seccion de Usuarios solo se puede editar/ver a si mismo pero no eliminar
            if ('DELETE' !== $name && $user->hasRolePrincipal(\Sonata\UserBundle\Model\UserInterface::ROLE_DEFAULT)) {
                if ($object && ($object->getId() !== $user->getId())) {
                    $isGranted = false;
                }
            }
        }

        return $isGranted;
    }

    ////////////////////////////////////////////////////////////////////////////

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper): void {
        $listMapper
                ->add('username', null, [
                    'label' => 'list.label_username',
                ])
                ->add('email', null, [
                    'label' => 'list.label_email',
                ])
                ->add('enabled', null, [
                    'editable' => true,
                    'label' => 'list.label_enabled',
                ])
                ->add('createdAt', null, [
                    'label' => 'list.label_created_at',
                ])
        ;

        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                    ->add('impersonating', 'string', [
                        'label' => 'list.label_impersonating',
                        'template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig',
                    ])
            ;
        }

        $listMapper
                ->add('_action', null, [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => [],
                    ],
                    'label' => 'td_action',
                    'translation_domain' => 'SonataAdminBundle',
                ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper): void {
        $filterMapper
                ->add('username', null, [
                    'label' => 'filter.label_username',
                ])
                ->add('email', null, [
                    'label' => 'filter.label_email',
                ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void {
        $this->getSubject()->setRole($this->trans('label_' . strtolower($this->getSubject()->getRole())));

        $showMapper
                ->add('username', null, [
                    'label' => 'show.label_username',
                ])
                ->add('role', 'text', [
                    'label' => 'show.label_role',
                ])
                ->add('enabled', 'boolean', [
                    'label' => 'show.label_enabled',
                ])
                ->add('firstname', null, [
                    'label' => 'show.label_firstname',
                ])
                ->add('lastname', null, [
                    'label' => 'show.label_lastname',
                ])
                ->add('email', null, [
                    'label' => 'show.label_email',
                ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void {
        $formMapper
                ->add('username', null, $this->getFormFieldsConfig('username'))
                ->add('role', 'choice', $this->getFormFieldsConfig('role'))
                ->add('enabled', null, [
                    'attr' => ['container-class' => 'col-md-4'],
                    'label' => 'form.label_enabled',
                ])
                ->add('firstname', null, [
                    'attr' => ['container-class' => 'col-md-4'],
                    'label' => 'form.label_firstname',
                    'required' => true,
                ])
                ->add('lastname', null, [
                    'attr' => ['container-class' => 'col-md-4'],
                    'label' => 'form.label_lastname',
                    'required' => true,
                ])
                ->add('email', null, [
                    'attr' => ['container-class' => ($this->getCurrentUser()->isSuperAdmin()) ? 'col-md-4' : 'col-md-12'],
                    'label' => 'form.label_email',
                    'required' => true,
                ])
                ->add('plainPassword', 'repeated', [
                    'invalid_message' => 'password.dont_match',
                    'first_options' => [
                        'attr' => [
                            'container-class' => 'col-md-6'
                        ],
                        'label' => ($this->isNew()) ? 'form.password' : 'title_user_edit_password',
                    ],
                    'second_options' => [
                        'attr' => [
                            'container-class' => 'col-md-6'
                        ],
                        'label' => 'form.password_confirmation',
                    ],
                    'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                    'translation_domain' => $this->getTranslationDomain(),
                    'type' => 'password',
                ])
        ;
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Devuelve la configuracion para los campos del formulario segun ciertas circunstancias
     * 
     * @param str $field Campo que se va a configurar
     * 
     * @return array
     */
    private function getFormFieldsConfig($field) {
        $config = [];

        switch ($field) {
            case 'username':
                $config = [
                    'label' => 'form.label_username',
                ];

                if ($this->getSubject()->getId()) {
                    $config = array_merge($config, [
                        'attr' => ['container-class' => 'col-md-4', 'readonly' => 'readonly'],
                        'data' => $this->getSubject()->getUsername(),
                        'mapped' => false,
                        'required' => false,
                    ]);
                } else {
                    $config = array_merge($config, [
                        'attr' => ['container-class' => 'col-md-4'],
                        'required' => true,
                    ]);
                }

                break;

            case 'role':
                $availableChoices = \Application\Sonata\UserBundle\Entity\User::$availableRoles;

                if (!($this->getCurrentUser()->isSuperAdmin())) {
                    $availableChoices = array_flip($availableChoices);

                    unset($availableChoices[\Sonata\UserBundle\Model\UserInterface::ROLE_SUPER_ADMIN]);

                    $availableChoices = array_flip($availableChoices);
                }

                $config = [
                    'attr' => ['container-class' => 'col-md-4'],
                    'choices' => $availableChoices,
                    'choice_translation_domain' => $this->getTranslationDomain(),
                    'label' => 'form.label_role',
                ];

                break;
        }

        return $config;
    }

}
