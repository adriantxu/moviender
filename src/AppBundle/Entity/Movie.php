<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Movie
 *
 * @ORM\Table(name="movie")
 * @ORM\Entity(repositoryClass="\AppBundle\Repository\MovieRepository")
 */
class Movie extends \AppBundle\Entity\AbstractEntity {

    /**
     * @var float
     *
     * @ORM\Column(name="popularity", type="float", options={"default": 0})
     */
    private $popularity;

    /**
     * @var integer
     *
     * @ORM\Column(name="vote_count", type="integer", options={"default": 0})
     */
    private $voteCount;

    /**
     * @var float
     *
     * @ORM\Column(name="vote_avg", type="float", options={"default": 0})
     */
    private $voteAvg;

    /**
     * @var boolean
     *
     * @ORM\Column(name="video", type="boolean", options={"default": 0})
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="poster_path", type="string", nullable=true)
     */
    private $posterPath;

    /**
     * @var integer
     *
     * @ORM\Column(name="movie_id", type="integer", unique=true)
     */
    private $movieId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="adult", type="boolean", options={"default": 0})
     */
    private $adult;

    /**
     * @var string
     *
     * @ORM\Column(name="backdrop_path", type="string", nullable=true)
     */
    private $backdropPath;

    /**
     * @var string
     *
     * @ORM\Column(name="original_language", type="string", options={"default": "en"})
     */
    private $originalLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="original_title", type="string")
     */
    private $originalTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="overview", type="text", nullable=true)
     */
    private $overview;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="release_date", type="datetime")
     */
    private $releaseDate;

    /**
     * @var \AppBundle\Entity\UserMovieLike
     * 
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\UserMovieLike", mappedBy="movie", cascade={"persist"})
     */
    private $userMovieLike;

    /**
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\MovieGenre", mappedBy="movies", cascade={"persist"})
     */
    private $genres;

    public function __construct() {
        parent::__construct();

        $this->popularity = 0;
        $this->voteCount = 0;
        $this->voteAvg = 0;
        $this->video = false;
        $this->adult = false;
        $this->originalLanguage = 'en';
        $this->genres = new ArrayCollection();
    }

    public function getPopularity() {
        return $this->popularity;
    }

    public function getVoteCount() {
        return $this->voteCount;
    }

    public function getVoteAvg() {
        return $this->voteAvg;
    }

    public function getVideo() {
        return $this->video;
    }

    public function getPosterPath() {
        return $this->posterPath;
    }

    public function getMovieId() {
        return $this->movieId;
    }

    public function getAdult() {
        return $this->adult;
    }

    public function getBackdropPath() {
        return $this->backdropPath;
    }

    public function getOriginalLanguage() {
        return $this->originalLanguage;
    }

    public function getOriginalTitle() {
        return $this->originalTitle;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getOverview() {
        return $this->overview;
    }

    public function getReleaseDate() {
        return $this->releaseDate;
    }

    public function getUserMovieLike() {
        return $this->userMovieLike;
    }

    public function getGenres() {
        return $this->genres;
    }

    public function setPopularity($popularity) {
        $this->popularity = $popularity;

        return $this;
    }

    public function setVoteCount($voteCount) {
        $this->voteCount = $voteCount;

        return $this;
    }

    public function setVoteAvg($voteAvg) {
        $this->voteAvg = $voteAvg;

        return $this;
    }

    public function setVideo($video) {
        $this->video = $video;

        return $this;
    }

    public function setPosterPath($posterPath) {
        $this->posterPath = $posterPath;

        return $this;
    }

    public function setMovieId($movieId) {
        $this->movieId = $movieId;

        return $this;
    }

    public function setAdult($adult) {
        $this->adult = $adult;

        return $this;
    }

    public function setBackdropPath($backdropPath) {
        $this->backdropPath = $backdropPath;

        return $this;
    }

    public function setOriginalLanguage($originalLanguage) {
        $this->originalLanguage = $originalLanguage;

        return $this;
    }

    public function setOriginalTitle($originalTitle) {
        $this->originalTitle = $originalTitle;

        return $this;
    }

    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    public function setOverview($overview) {
        $this->overview = $overview;

        return $this;
    }

    public function setReleaseDate(\DateTime $releaseDate) {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function setUserMovieLike(\AppBundle\Entity\UserMovieLike $userMovieLike = null) {
        $this->userMovieLike = $userMovieLike;

        return $this;
    }

    public function addGenre(\AppBundle\Entity\MovieGenre $genre = null) {
        if ($this->genres->contains($genre)) {
            return null;
        }

        $this->genres[] = $genre;

        $genre->addMovie($this);

        return $this;
    }

    public function removeGenre(\AppBundle\Entity\MovieGenre $genre = null) {
        if (!$this->genres->contains($genre)) {
            return null;
        }

        $this->genres->removeElement($genre);

        return $this;
    }

}
