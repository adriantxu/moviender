<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cinema
 *
 * @ORM\Table(name="cinema")
 * @ORM\Entity(repositoryClass="\AppBundle\Repository\CinemaRepository")
 */
class Cinema extends \AppBundle\Entity\AbstractEntity {

    /**
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="\Application\Sonata\UserBundle\Entity\User", inversedBy="cinemas", cascade={"persist"})
     */
    private $users;

    public function __construct() {
        parent::__construct();

        $this->users = new ArrayCollection();
    }

    public function getUsers() {
        return $this->users;
    }

    public function addUser(\Application\Sonata\UserBundle\Entity\User $user = null) {
        if ($this->users->contains($user)) {
            return null;
        }

        $this->users[] = $user;

        $user->addCinema($this);

        return $this;
    }

    public function removeUser(\Application\Sonata\UserBundle\Entity\User $user = null) {
        if (!$this->users->contains($user)) {
            return null;
        }

        $this->users->removeElement($user);

        $user->removeCinema($this);

        return $this;
    }

}
