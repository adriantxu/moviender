$(document).ready(function () {
    const keyPressEventName = 'keypress';

    // Agregamos el boton para añadir el elemento
    $('<a class="add-to-choice-btn" href="javascript:;"><i class="fa fa-plus"></i></a>').insertAfter('.add-to-choice');

    // Agregamos el binding para que cuando haga clic pulse la tecla Enter
    $('.add-to-choice').parent().on('click', '.add-to-choice-btn', function () {
        var e = $.Event(keyPressEventName);

        e.which = 13;

        $(this).parent().find('.add-to-choice').trigger(e);
    });

    // Agregamos la funcionalidad
    $('.add-to-choice').on(keyPressEventName, function (e) {
        if (13 === e.which) {
            e.preventDefault();
            e.stopPropagation();

            triggerSonataError($(this));

            // Si no se ha introducido texto no hacemos nada
            if ('' !== $(this).val()) {
                const value = $(this).val();

                // Si el type es email hay que comprobar que sea un correo
                if ('email' === $(this).attr("type")) {
                    let emailPattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

                    if (!emailPattern.test($(this).val())) {
                        triggerSonataError($(this), true, $(this).attr('data-message'));

                        return;
                    }
                }

                // Comprobamos que no exista la tag
                var tagExist = false;
                $('#' + $(this).attr('data-add-to') + ' option').filter(function () {
                    if (value.toLowerCase() === this.value.toLowerCase()) {
                        tagExist = true;
                    }
                });

                if (tagExist) {
                    // Si la tag existe mostramos el efecto highlight en el elemento existente
                    for (let i = 0; i < 3; i++) {
                        $('#s2id_' + $(this).attr('data-add-to') + ' > .select2-choices > .select2-search-choice').filter(function () {
                            if (value.toLowerCase() === $(this).find('div').text().toLowerCase()) {
                                $(this).effect('highlight', {color: '#dd4b39'}, 500);
                            }
                        });
                    }
                } else {
                    $('#' + $(this).attr('data-add-to')).append('<option value="' + value + '" selected="selected">' + value + '</option>');

                    $(this).val('');

                    // Recargamos la configuracion de los select2 para que se muestre el elemento recien incorporado
                    Admin.setup_select2(document);

                }
            }
        }
    });
});