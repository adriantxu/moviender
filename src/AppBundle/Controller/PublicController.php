<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/html")
 */
class PublicController extends Controller {

    /**
     * @Route("", name="public_login")
     */
    public function loginAction(Request $request) {
        return $this->render('AppBundle/Public/login/login.html.twig');
    }

    /**
     * @Route("/panel/dashboard", name="public_panel_dashboard")
     */
    public function dashboardAction(Request $request) {
        // MOVIE GENRE
        $genres = [];
        $items = $this->getDoctrine()->getRepository('AppBundle:MovieGenre')->findBy([], ['name' => 'ASC']);

        foreach ($items as $item) {
            $genres[] = [
                'id' => $item->getGenreId(),
                'name' => $item->getName(),
                'image' => $item->getThumbnail(),
            ];
        }

        // MY LIKES
        // @todo

        $params = [
            'genres' => $genres,
            'myLikes' => [],
        ];

        return $this->render('AppBundle/Public/panel/dashboard.html.twig', $params);
    }

}
