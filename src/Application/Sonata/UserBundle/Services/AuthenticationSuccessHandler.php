<?php

namespace Application\Sonata\UserBundle\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface {

    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
        // En caso de necesitar desloguear al usuario
        if (false) {
            $anonToken = new AnonymousToken('theTokensKey', 'anon.', []);
            $this->container->get('security.token_storage')->setToken($anonToken);

            $request->getSession()->getFlashBag()->add('error', $this->container->get('translator')->trans('access_denied', [], 'SonataAdminBundle'));
        }

        return new RedirectResponse($this->container->get('router')->generate('sonata_admin_dashboard'));
    }

}
