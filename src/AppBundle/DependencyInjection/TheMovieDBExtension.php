<?php

namespace AppBundle\DependencyInjection;

/**
 * @see https://developers.themoviedb.org/3/getting-started/introduction
 */
class TheMovieDBExtension {

    const API_KEY = 'c2417a53a7cd2f4129291f3f1ab8278e';
    const API_KEY_STR = 'api_key';
    const API_VERSION = 3;
    const API_URL = 'https://api.themoviedb.org';
    const POSTER_PATH = 'https://image.tmdb.org/t/p/';

    /**
     * Llama a un endpoint de TheMovieDB y devuelve el resultado
     * 
     * @param string  $url    Endpoint al que se llama
     * @param array   $params Parametros que iran en la llamada query
     * @param integer $adult  Opcinal. Por defecto devolvemos las peliculas para adultos
     * 
     * @return array
     */
    public static function callUrl(string $url, array $params = [], $adult = 1, $video = 0) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
            CURLOPT_ENCODING => '', // handle compressed
            // CURLOPT_USERAGENT => 'test', // name of client
            CURLOPT_AUTOREFERER => true, // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120, // time-out on connect
            CURLOPT_TIMEOUT => 120, // time-out on response
        );

        // Agregamos los parametros extra
        $params['include_adult'] = $adult;
        $params['include_video'] = $video;

        // Concatenamos la key del array params con su value para formas los parametros que iran en la query
        $paramsStr = '';

        foreach ($params as $key => $value) {
            $paramsStr .= '&' . $key . '=' . $value;
        }

        $ch = curl_init(self::API_URL . '/' . self::API_VERSION . '/' . $url . '?' . self::API_KEY_STR . '=' . self::API_KEY . $paramsStr);

        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);

        curl_close($ch);

        return json_decode($content, true);
    }

    /**
     * Devuelve la ruta completa del poster
     * 
     * @param string  $posterPath
     * @param integer $width
     * @param integer $height
     * 
     * @return string
     */
    public static function getPosterPath($posterPath, $width = 440, $height = 660) {
        return self::POSTER_PATH . 'w' . $width . '_and_h' . $height . '_face/' . $posterPath;
    }

}
