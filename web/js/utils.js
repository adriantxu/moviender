/**
 * Devuelve el uniqid del formulario, que se utiliza para nombrar los campos
 * 
 * @returns {string}
 */
function getFormId() {
    if ($('.content-wrapper .sonata-ba-form > form').length > 1) {
        console.log('Solo puede haber un unico formulario!');
    }

    return $('.content-wrapper .sonata-ba-form > form').attr('action').split('uniqid=')[1];
}
;