// @see https://ckeditor.com/latest/samples/toolbarconfigurator/index.html#advanced

function initCkeditorMin() {
    $('.ckeditor-mini').each(function (i) {
        var id = $(this).attr('id');

        CKEDITOR.replace(id, {
            forcePasteAsPlainText: true,
            language: $('html').attr('lang') || 'es',
            toolbar: [
                ['Format', 'Font', 'FontSize'],
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['ImageButton'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
                ['Undo', 'Redo'],
                ['Bold', 'Italic']
            ]
        });
    });
}
;

function initCkeditorRegular() {
    $('.ckeditor-regular').each(function (i) {
        var id = $(this).attr('id');

        CKEDITOR.replace(id, {
            forcePasteAsPlainText: true,
            language: $('html').attr('lang') || 'es',
            toolbar: [
                ['Format', 'Font', 'FontSize'],
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['ImageButton'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
                ['Undo', 'Redo'],
                ['Bold', 'Italic']
            ]
        });
    });
}
;

function initCkeditorFull() {
    $('.ckeditor-full').each(function (i) {
        var id = $(this).attr('id');

        CKEDITOR.replace(id, {
            forcePasteAsPlainText: true,
            language: $('html').attr('lang') || 'es',
            toolbar: [
                ['Format', 'Font', 'FontSize'],
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['ImageButton'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
                ['Undo', 'Redo'],
                ['Bold', 'Italic']
            ]
        });
    });
}
;

$(document).ready(function () {
    initCkeditorMin();
    initCkeditorRegular();
    initCkeditorFull();
});