<?php

namespace AppBundle\Repository;

use AppBundle\DependencyInjection\UtilsExtension;

class AbstractRepository extends \Doctrine\ORM\EntityRepository {

    protected $dqlTable;

    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, \Doctrine\ORM\Mapping\ClassMetadata $class) {
        parent::__construct($em, $class);

        $this->dqlTable = str_replace('\\\\', ':', str_ireplace('repository', '', get_class($this)));
    }

    /**
     * Rellena una Entity con los parametros pasados
     * 
     * @param mixed $entity Entity que se va a rellenar
     * @param array $params Parametros con los valores
     * 
     * @return mixed
     */
    public function arrayToEntity($entity, $params) {
        $reflectionClass = new \ReflectionClass(get_class($entity));

        foreach ($params as $key => $param) {
            $save = true;

            $setMethod = 'set' . ucfirst($key);

            // Para las relaciones onetomany
            $addMethod = 'add' . rtrim(ucfirst($key), 's');

            $setMethodExists = method_exists($entity, $setMethod);
            $addMethodExists = method_exists($entity, $addMethod);
            $propertyExists = $reflectionClass->hasProperty($key);

            if (($setMethodExists || $addMethodExists) && ($propertyExists)) {
                $property = $reflectionClass->getProperty($key);

                $getMethod = 'get' . ucfirst($property->name);

                if (method_exists($entity, $getMethod)) {
                    $propertyType = UtilsExtension::getStrBetween($property->getDocComment(), 'type="', '"');

                    if ($propertyType) {
                        switch (strtolower($propertyType)) {
                            case 'time':
                            case 'datetime':
                                $param = new \DateTime($param, new \DateTimeZone('Europe/Madrid'));

                                break;
                        }
                    } else {
                        // Tiene que ser una relacion OneToOne, OneToMany o ManyToOne
                        // Las OneToMany van a ser un ArrayCollecion, las demas son una instancia del objeto relacionado
                        $relatedType = UtilsExtension::getStrBetween($property->getDocComment(), '@ORM\\', '(');

                        switch (strtolower($relatedType)) {
                            case 'onetoone':
                            case 'manytoone':
                                $relatedClass = UtilsExtension::getStrBetween($property->getDocComment(), 'targetEntity="', '"');

                                $param = (isset($param['id'])) ? $param['id'] : $param;

                                if (!is_numeric($param)) {
                                    $save = false;
                                    // En este punto el objeto esta mal construido y no tiene un ID por el que buscar
                                } else {
                                    $param = $this->getEntityManager()->find($relatedClass, $param);
                                }

                                break;

                            case 'onetomany':
                                if (is_array($param)) {
                                    $relatedClassName = UtilsExtension::getStrBetween($property->getDocComment(), 'targetEntity="', '"');

                                    foreach ($param as $p) {
                                        $relatedClass = new $relatedClassName();

                                        $item = $this->arrayToEntity($relatedClass, $p);

                                        $entity->$addMethod($item);

                                        $this->_em->persist($item);
                                    }
                                }
                        }
                    }

                    // Es posible que haya entrado en la opcion OneToMany y en ese caso no hay que hacer un setMethod
                    if ($save && method_exists($entity, $setMethod)) {
                        $entity->$setMethod($param);
                    }
                }
            }
        }

        return $entity;
    }

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Devuelve el nombre de la tabla
     * 
     * @return string
     */
    final protected function getSqlTable() {
        return $this->_class->table['name'];
    }

}
