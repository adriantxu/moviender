function stringifyJsonForGood(_data) {
    $.each(_data, function (index, value) {
        if ('undefined' === typeof value) {
            _data[index] = 'undefined';
        }
    });

    return _data;
//    return JSON.stringify(_data);
}
;

function validateJsonIntegrity(_data) {
    var deserialized = $.parseJSON(_data);
    var validJson = true;

    $.each(deserialized, function (index, value) {
        if ('undefined' === value) {
            if (isDev()) {
                console.log('=======================================================================');
                console.log('                                                                       ');
                console.log('     Eh, la variable: ' + index + ' te la has dejado en undefined!     ');
                //console.log('     Funcion llamada desde ' + baseAjax.caller +                 '     ');
                console.log('                                                                       ');
                console.log('=======================================================================');
            }

            validJson = false;
        }
    });

    return validJson;
}
;

/**
 * Llamada ajax de base, para no tener que andar repitiendo todo el codigo
 * 
 * Los parametros _url y _data son obligatorios
 * 
 * @param {string}             _url Url donde se va a hacer la peticion
 * @param {string}             _data Datos que se pasan
 * @param {string}             _type Tipo de llamada (POST/GET)
 * @param {string}             _contentType Mime del contenido
 * @param {string}             _dataType Tipo de contenido que se pasa
 * @param {bool}               _processData Si es necesario procesar el contenido
 * @param {callable|bool|null} _doneCallback Callback cuando ha terminado la llamada
 * @param {callable|bool|null} _successCallback Callback cuando la llamada ha salido bien
 * @param {callable|bool|null} _failCallback Callback cuando ha fallado la llamada
 * @param {callable|bool|null} _alwaysCallback Callback cuando se termina todo el flujo
 * 
 * @returns {undefined}
 */
function baseAjax(_url, _data, _type, _contentType, _dataType, _processData, _doneCallback, _successCallback, _failCallback, _alwaysCallback) {
    var result = {
        'isSuccess': false
    };

    result.isSuccess = false;

    // Configuramos los parametros
    _type = _type || 'post';
    _contentType = _contentType || 'application/json';
    _data = (_data) ? stringifyJsonForGood(_data) : false;
    _dataType = _dataType || 'json';
    _processData = _processData || false;
    _successCallback = _successCallback || false;
    _doneCallback = _doneCallback || false;
    _failCallback = _failCallback || false;
    _alwaysCallback = _alwaysCallback || false;

    // Antes de hacer nada, vamos a comprobar que todos los parametros de la variable de tipo json _data no esten en 'undefined'.
    // Si estan a 'null' en principio no pasa nada porque esto puede ser asi, pero 'undefined' suena a que la variable se ha recogido mal desde javascript
    var validJson = true;

    if ('json' === _dataType && _processData) {
        validJson = validateJsonIntegrity(_data);
    }

    if (validJson) {
        $.ajax({
//            async: false, // Esperamos a que termine toda la accion de la llamada ajax para devolver el valor de lo sucedido en los controladores
//            contentType: _contentType,
//            dataType: _dataType,
            data: _data,
//            processData: _processData,
            type: _type,
            url: _url,
            beforeSend: function () {
                // Agregamos la clase cursor-wait al html para que el usuario vea que se esta haciendo algo
                $('html').addClass('cursor-wait');
            },
            success: function (data) {
                // Si recibimos texto tenemos que adecuar la salida
                if ('string' === typeof (data)) {
                    result.data = data;
                    result.isSuccess = true;
                } else {
                    result = data;
                }

                if (_successCallback) {
                    _successCallback(result);
                }
            }
        }).done(function (data) {
            console.log('Done');
            if (_doneCallback) {
                _doneCallback(data);
            }
        }).fail(function (data) {
            result.isSuccess = false;

            console.log('Fail');

            // Si estamos en dev mostramos el error de una manera mas visual
            if (isDev()) {
                var modal = '<div class="modal show" style="overflow: auto;"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title">AJAX ERROR</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).closest(' + "'.modal'" + ').remove();"><span aria-hidden="true">&times;</span></button></div><div class="modal-body" id="modal-base-ajax-fail"></div></div></div></div>';
                var iframe = document.createElement('iframe');
                var html = data.responseText;
                iframe.setAttribute('style', 'width: 100%; height: 1500px;');

                $('body').append(modal);
                $('#modal-base-ajax-fail').append(iframe);
                iframe.contentWindow.document.open();
                iframe.contentWindow.document.write(html);
                iframe.contentWindow.document.close();
            }

            if (_failCallback) {
                _failCallback(data.responseJSON);
            }
        }).always(function (data) {
            if (_alwaysCallback) {
                _alwaysCallback(data.responseJSON);
            }

            // Eliminamos la clase cursor-wait
            $('html').removeClass('cursor-wait');
        });
    } else {
        // Si estamos en dev mostramos el error de una manera mas visual
        if (isDev()) {
            $('body').append('<div class="modal show"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title">Json integrity fail!</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$(this).closest(' + "'.modal'" + ').remove();"><span aria-hidden="true">&times;</span></button></div><div class="modal-body" id="modal-base-ajax-json-fail"><div class="alert alert-error">Unknown error while parsing json! Please check json integrity.</div></div></div></div></div>');
        }
    }

    return result;
}
;