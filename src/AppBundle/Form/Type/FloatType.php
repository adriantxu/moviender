<?php

// src/AppBundle/Form/Type/FloatType.php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class FloatType extends AbstractType {

    public function getParent() {
        return NumberType::class;
    }

}
