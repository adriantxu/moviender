<h1>Moviender</h1>
-------------------------

Cansado de quedar con tus amigos para ver una película y estar 1 hora para decidir qué queréis ver?
Eso se acabó! Con Moviender puedes navegar por un listado de películas y marcar cuáles te han gustado,
cuáles no te importaría volver a ver y las que no recomendarías ni a tus peores enemigos.
En el momento en el que conectas con una o más personas en una sala,
aparecerá un listado de las películas que ambos habéis marcado con un like anteriormente,
así podréis tener más a mano un listado de películas listas para ver!!!
