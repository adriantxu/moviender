<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener {

    protected $em;

    function __construct($serviceContainer, EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    public function onKernelRequest(GetResponseEvent $event) {
        $request = $event->getRequest();

        // Comprobamos si existe un usuario para poder sacar su idioma de la app
        if (unserialize($request->getSession()->get('_security_user'))) {
            $user = unserialize($request->getSession()->get('_security_user'))->getUser();

            if ($user instanceof \Application\Sonata\UserBundle\Entity\User) {
                $locale = $this->em->getConnection()->fetchColumn('SELECT idioma FROM fos_user_user WHERE email = ?;', [$user->getEmail()]);

                $request->getSession()->set('_locale', $locale);

                $_SESSION['idioma'] = $locale;
            }
        }
    }

}
