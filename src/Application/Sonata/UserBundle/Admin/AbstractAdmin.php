<?php

namespace Application\Sonata\UserBundle\Admin;

use Sonata\UserBundle\Admin\Model\UserAdmin as ParentAbstractAdmin;

class AbstractAdmin extends ParentAbstractAdmin {

    protected $translationDomain = 'SonataUserBundle';

    /**
     * @final Este metodo es necesario para cargar los JS y CSS en las plantillas twig, no hay que modificarlo, ni heredarlo ni hacer nada mas con el
     * 
     * @todo El metodo se repite en src/AppBundle/Admin/Abstract/Admin. Hay que buscar una forma de no duplicar el metodo
     * 
     * Devolvemos la propiedad formOptions
     * 
     * @return array
     */
    final public function getFormOptions() {
        return $this->formOptions;
    }

    /**
     * Devuelve el usuario logueado
     * 
     * @todo El metodo se repite en src/AppBundle/Admin/Abstract/Admin. Hay que buscar una forma de no duplicar el metodo
     * 
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    final public function getCurrentUser() {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * Actualizado el valor de validation_groups por User para que el Admin UserAdmin puede utilizar el metodo validate
     * 
     * {@inheritdoc}
     */
    public function getFormBuilder() {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $options['validation_groups'] = 'User';

        $formBuilder = $this->getFormContractor()->getFormBuilder($this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * Si no existe el userManager creamos uno
     * 
     * @return UserManagerInterface
     */
    public function getUserManager() {
        if (!$this->userManager) {
            $this->setUserManager($this->getConfigurationPool()->getContainer()->get('fos_user.user_manager'));
        }

        return $this->userManager;
    }
    
    /**
     * No dejamos heredar el metodo para que no se salte la comprobacion del metodo preventDelete
     * 
     * @param \Application\Sonata\UserBundle\Entity\User $object
     */
    final public function preRemove($object) {
        if (true === $this->preventDelete($object->getId())) {
            $redirection = new \Symfony\Component\HttpFoundation\RedirectResponse($this->generateUrl('list'));
            $redirection->send();

            die;
        }
    }

    /**
     * No dejamos heredar el metodo para que no se salte la comprobacion del metodo preventDelete
     * 
     * @param str                                              $actionName
     * @param \Sonata\AdminBundle\Datagrid\ProxyQueryInterface $query
     * @param array                                            $idx
     * @param boolean                                          $allElements
     */
    final public function preBatchAction($actionName, \Sonata\AdminBundle\Datagrid\ProxyQueryInterface $query, array &$idx, $allElements) {
        if ('delete' === $actionName) {
            $ids = $idx;

            if ($allElements && empty($ids)) {
                $items = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository($this->getClass())->findAll();

                $ids = array_map(create_function('$e', 'return $e->getId();'), $items);
            }

            foreach ($ids as $id) {
                if (true === $this->preventDelete($id)) {
                    $redirection = new \Symfony\Component\HttpFoundation\RedirectResponse($this->generateUrl('list'));
                    $redirection->send();

                    die;
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Comprobamos si el objeto es nuevo o ya existe
     * 
     * @return boolean
     */
    final protected function isNew() {
        return (null === $this->getSubject()) ? true : ((null === $this->getSubject()->getId()) ? true : false);
    }
    
    /**
     * Por defecto devolvemos false para que se pueda eliminar el objeto
     * 
     * Las clases que necesiten hacer una comprobacion tendran que sobrecargar este metodo
     * 
     * @param int $id
     */
    protected function preventDelete($id) {
        return false;

        $elemento = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository($this->getClass())->find($id);
    }

}
