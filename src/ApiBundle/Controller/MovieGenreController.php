<?php

namespace ApiBundle\Controller;

use ApiBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("movie/genre/", name="api_movie_genre")
 */
class MovieGenreController extends AbstractController {

    /**
     * Devuelve todos los MovieGenre
     * 
     * @Route("all", name="api_movie_genre_all", methods={"POST"})
     */
    public function getAllMovieGenreAction(Request $request) {
        if ($token = $this->getToken($request)) {
            if ($user = $this->getUserByToken($token)) {
                $data = [];

                $items = $this->getDoctrine()->getRepository('AppBundle:MovieGenre')->findAll();

                foreach ($items as $item) {
                    $data[] = [
                        'id' => $item->getGenreId(),
                        'name' => $item->getName(),
                        'thumbnail' => $item->getThumbnail(),
                    ];
                }

                $this->setDefaultSuccessResponse(null, $data);
            }
        }

        return new JsonResponse($this->responseData, $this->responseStatus);
    }

    // @todo Hacer un metodo que compruebe periodicamente si hay nuevos MovieGenre?
}
