<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserMovieLike
 *
 * @ORM\Table(name="user_movie_like",
 * uniqueConstraints={
 *        @ORM\UniqueConstraint(
 *            columns={"value", "user_id", "movie_id"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="\AppBundle\Repository\UserMovieLikeRepository")
 */
class UserMovieLike extends \AppBundle\Entity\AbstractEntity {

    protected $name = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="value", type="boolean", options={"default": 0})
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", inversedBy="userMovieLike", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Movie", inversedBy="userMovieLike", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $movie;

    public function __construct() {
        parent::__construct();

        $this->value = false;
    }

    public function getValue() {
        return $this->value;
    }

    public function getUser() {
        return $this->user;
    }

    public function getMovie() {
        return $this->movie;
    }

    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    public function setUser($user) {
        $this->user = $user;

        return $this;
    }

    public function setMovie($movie) {
        $this->movie = $movie;

        return $this;
    }

}
