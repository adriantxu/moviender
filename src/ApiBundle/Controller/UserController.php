<?php

namespace ApiBundle\Controller;

use ApiBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController {

    /**
     * Login del usuario
     * 
     * @Route("login", name="api_login", methods={"POST"})
     */
    public function loginAction(Request $request) {
        foreach ([
    'username',
    'password',
        ] as $requiredParam) {
            if (!($$requiredParam = $request->request->get($requiredParam))) {
                $this->setDefaultErrorResponse($this->translate('Api', 'errors.mandatory_param', ['%PARAMETRO%' => $requiredParam]));

                return new JsonResponse($this->responseData, $this->responseStatus);
            }
        }

        try {
            $user = $this->get('fos_user.user_manager')->findUserByUsername($username);

            // Si no existe el usuario sacamos un error
            if (!$user instanceof \Application\Sonata\UserBundle\Entity\User) {
                throw new \Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
            }

            // Comprobamos que el usuario haya introducido bien el username y la contraseña
            if (false === $this->get('security.password_encoder')->isPasswordValid($user, $password)) {
                throw new \Symfony\Component\Security\Core\Exception\BadCredentialsException();
            } else {
                // Comprobamos que el usuario este activo
                if (true !== $user->isEnabled()) {
                    throw new \Symfony\Component\Security\Core\Exception\DisabledException();
                } else {
                    // Actualizamos la fecha del ultimo login y creamos un nuevo token
                    $user->setLastLogin(new \DateTime());
                    $user->setToken(rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '='));

                    $this->get('fos_user.user_manager')->updateUser($user, true);

                    $this->setDefaultSuccessResponse(null, ['username' => $user->getUsername(), 'token' => $user->getToken()]);
                }
            }
        } catch (\Symfony\Component\Security\Core\Exception\UsernameNotFoundException $e) {
            $this->setDefaultErrorResponse($this->translate('Api', 'errors.login.not_found_user'));
        } catch (\Symfony\Component\Security\Core\Exception\DisabledException $e) {
            $this->setDefaultErrorResponse($this->translate('Api', 'errors.login.account_disabled'));
        } catch (\Symfony\Component\Security\Core\Exception\BadCredentialsException $e) {
            $this->setDefaultErrorResponse($this->translate('Api', 'errors.login.incorrect_user_password'));
        } catch (\Exception $e) {
            $this->setDefaultErrorResponse($this->translate('SonataAdminBundle', 'unknown_error', [], true, 'xliff'));
        }

        return new JsonResponse($this->responseData, $this->responseStatus);
    }

    /**
     * Logout del usuario
     * 
     * @Route("logout", name="api_logout", methods={"POST"})
     */
    public function logoutAction(Request $request) {
        if ($token = $this->getToken($request)) {
            if ($user = $this->getUserByToken($token)) {
                $user->setToken(null);

                $this->get('fos_user.user_manager')->updateUser($user, true);

                $this->setDefaultSuccessResponse();
            }
        }

        return new JsonResponse($this->responseData, $this->responseStatus);
    }

    /**
     * Cambio de contraseña
     * 
     * @Route("change_password", name="api_change_password", methods={"POST"})
     */
    public function changePasswordAction(Request $request) {
        if ($token = $this->getToken($request)) {
            if ($user = $this->getUserByToken($token)) {
                
            }
        }

        return new JsonResponse($this->responseData, $this->responseStatus);
    }

    /**
     * Comprueba si viene el token en las cabeceras y si ese token pertenece a un usuario activo
     * 
     * @Route("check_token", name="api_check_token", methods={"POST"})
     */
    public function checkTokenAction(Request $request) {
        if ($token = $this->getToken($request)) {
            if ($user = $this->getUserByToken($token)) {
                if ($user->isEnabled()) {
                    $this->setDefaultSuccessResponse();
                } else {
                    $this->setDefaultErrorResponse($this->translate('Api', 'errors.login.account_disabled'));
                }
            }
        }

        return new JsonResponse($this->responseData, $this->responseStatus);
    }

}
