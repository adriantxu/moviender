<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exception\LockException;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Sonata\AdminBundle\Util\AdminObjectAclData;
use Sonata\AdminBundle\Util\AdminObjectAclManipulator;
use Symfony\Bridge\Twig\AppVariable;
use Symfony\Bridge\Twig\Command\DebugCommand;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AbstractAdminController extends CRUDController {

    protected $translationDomain = 'messages';

    /**
     * Idioma por defecto
     *
     * @var string
     */
    protected $lang = 'es';

    /**
     * Response basico
     *
     * @var array
     */
    protected $response = [
        'success' => false,
        'message' => 'NO DIRECT ACCESS ALLOWED',
        'data' => null,
        'status' => 405,
    ];

    /**
     * List action.
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response
     */
    public function listAction() {
        $request = $this->getRequest();

        try {
            $this->admin->checkAccess('list');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.title', [], 'messages'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        }

        $preResponse = $this->preList($request);
        if (null !== $preResponse) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFilterTheme());

        return $this->renderWithExtraParams($this->admin->getTemplate('list'), [
                    'action' => 'list',
                    'form' => $formView,
                    'datagrid' => $datagrid,
                    'csrf_token' => $this->getCsrfToken('sonata.batch'),
                    'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                    $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                    $this->admin->getExportFormats(),
                        ], null);
    }

    public function batchAction() {
        try {
            return parent::batchAction();
        } catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            // Si hacemos una llamada GET va a dar error asi que redirigimos al listado
            $this->addFlash('sonata_flash_error', $e->getMessage());

            return $this->redirectToList();
        } catch (\Exception $e) {
            // Ante cualquier error redirigimos al listado
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirectToList();
        }
    }

    /**
     * Execute a batch delete.
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return RedirectResponse
     */
    public function batchActionDelete(ProxyQueryInterface $query) {
        try {
            $this->admin->checkAccess('batchDelete');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.title', [], 'messages'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirect($this->admin->generateObjectUrl('list', $this->admin));
        }

        try {
            $query->select('DISTINCT ' . current($query->getRootAliases()));

            try {
                $entityManager = $this->admin->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

                $i = 0;
                foreach ($query->getQuery()->iterate() as $pos => $object) {
                    $this->admin->delete($object[0]);

                    if (0 == ( ++$i % 20)) {
                        $entityManager->flush();
                        $entityManager->clear();
                    }
                }

                $entityManager->flush();
                $entityManager->clear();
            } catch (\PDOException $e) {
                throw new ModelManagerException('', 0, $e);
            } catch (DBALException $e) {
                throw new ModelManagerException('', 0, $e);
            }
            $this->addFlash('sonata_flash_success', $this->trans('flash_batch_delete_success', [], 'SonataAdminBundle'));
        } catch (ModelManagerException $e) {
            $this->handleModelManagerException($e);
            $this->addFlash(
                    'sonata_flash_error', $this->trans('flash_batch_delete_error', [], 'SonataAdminBundle'));
        }

        return $this->redirectToList();
    }

    /**
     * Delete action.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function deleteAction($id) {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        try {
            $this->admin->checkAccess('delete', $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $preResponse = $this->preDelete($request, $object);
        if (null !== $preResponse) {
            return $preResponse;
        }

        if ('DELETE' == $this->getRestMethod()) {
            // check the csrf token
            $this->validateCsrfToken('sonata.delete');

            $objectName = $this->admin->toString($object);

            try {
                $this->admin->delete($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(['result' => 'ok'], 200, []);
                }

                $this->addFlash('sonata_flash_success', $this->trans('flash_delete_success', ['%name%' => $this->escapeHtml($objectName)], 'SonataAdminBundle'));
            } catch (ModelManagerException $e) {
                $this->handleModelManagerException($e);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(['result' => 'error'], 200, []);
                }

                $this->addFlash('sonata_flash_error', $this->trans('flash_delete_error', ['%name%' => $this->escapeHtml($objectName)], 'SonataAdminBundle'));
            } catch (\Exception $e) {
                $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));
            }

            return $this->redirectTo($object);
        }

        return $this->renderWithExtraParams($this->admin->getTemplate('delete'), [
                    'object' => $object,
                    'action' => 'delete',
                    'csrf_token' => $this->getCsrfToken('sonata.delete'),
                        ], null);
    }

    /**
     * Edit action.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function editAction($id = null) {
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $request->get($this->admin->getIdParameter());
        $existingObject = $this->admin->getObject($id);

        if (!$existingObject) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $existingObject));
        }

        try {
            $this->admin->checkAccess('edit', $existingObject);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirect($this->admin->generateObjectUrl('list', $existingObject));
        }

        $preResponse = $this->preEdit($request, $existingObject);
        if (null !== $preResponse) {
            return $preResponse;
        }

        $this->admin->setSubject($existingObject);
        $objectId = $this->admin->getNormalizedIdentifier($existingObject);

        /** @var $form Form */
        $form = $this->admin->getForm();
        $form->setData($existingObject);

        try {
            $form->handleRequest($request);
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $e->getMessage());

            $formView = $form->createView();
            // set the theme for the current Admin Form
            $this->setFormTheme($formView, $this->admin->getFormTheme());

            return $this->renderWithExtraParams($this->admin->getTemplate($templateKey), [
                        'action' => 'edit',
                        'form' => $formView,
                        'object' => $existingObject,
                        'objectId' => $objectId,
                            ], null);
        }

        if ($form->isSubmitted()) {
            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($existingObject);
            }
            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $submittedObject = $form->getData();
                $this->admin->setSubject($submittedObject);

                try {
                    $existingObject = $this->admin->update($submittedObject);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson([
                                    'result' => 'ok',
                                    'objectId' => $objectId,
                                    'objectName' => $this->escapeHtml($this->admin->toString($existingObject)),
                                        ], 200, []);
                    }

                    $this->addFlash('sonata_flash_success', $this->trans('flash_edit_success', ['%name%' => $this->escapeHtml($this->admin->toString($existingObject))], 'SonataAdminBundle'));

                    // redirect to list mode
                    return $this->redirectTo($existingObject);
                } catch (ModelManagerException $e) {
                    // Si el mensaje anterior es de tipo \Doctrine\DBAL\Exception\UniqueConstraintViolationException significa que un valor del formulario ya existe y esta configurado para que sea unico
                    // Hay que aislar el valor para sacar el mensaje por pantalla
                    if ($e->getPrevious() instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException) {
                        $message = $e->getPrevious()->getMessage();

                        $strSplit = explode('SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry ', $message);
                        $strSplit = explode(' for key', $strSplit[1]);

                        $this->addFlash('error', $this->trans('value.duplicated', ['%VALUE%' => $strSplit[0]], 'SonataCoreBundle'));
                    } else {
                        $this->handleModelManagerException($e);
                    }

                    $isFormValid = false;
                } catch (LockException $e) {
                    $this->addFlash('sonata_flash_error', $this->trans('flash_lock_error', [
                                '%name%' => $this->escapeHtml($this->admin->toString($existingObject)),
                                '%link_start%' => '<a href="' . $this->admin->generateObjectUrl('edit', $existingObject) . '">',
                                '%link_end%' => '</a>',
                                    ], 'SonataAdminBundle'));
                } catch (\Exception $e) {
                    $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', $this->trans('flash_edit_error', ['%name%' => $this->escapeHtml($this->admin->toString($existingObject))], 'SonataAdminBundle'));
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $formView = $form->createView();
        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFormTheme());

        return $this->renderWithExtraParams($this->admin->getTemplate($templateKey), [
                    'action' => 'edit',
                    'form' => $formView,
                    'object' => $existingObject,
                    'objectId' => $objectId,
                        ], null);
    }

    /**
     * Create action.
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response
     */
    public function createAction() {
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        try {
            $this->admin->checkAccess('create');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.title', [], 'messages'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $class = new \ReflectionClass($this->admin->hasActiveSubClass() ? $this->admin->getActiveSubClass() : $this->admin->getClass());

        if ($class->isAbstract()) {
            return $this->renderWithExtraParams(
                            '@SonataAdmin/CRUD/select_subclass.html.twig', [
                        'base_template' => $this->getBaseTemplate(),
                        'admin' => $this->admin,
                        'action' => 'create',
                            ], null
            );
        }

        $newObject = $this->admin->getNewInstance();

        $preResponse = $this->preCreate($request, $newObject);
        if (null !== $preResponse) {
            return $preResponse;
        }

        $this->admin->setSubject($newObject);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($newObject);

        try {
            $form->handleRequest($request);
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $e->getMessage());

            $formView = $form->createView();
            // set the theme for the current Admin Form
            $this->setFormTheme($formView, $this->admin->getFormTheme());

            return $this->renderWithExtraParams($this->admin->getTemplate($templateKey), [
                        'action' => 'create',
                        'form' => $formView,
                        'object' => $newObject,
                        'objectId' => null,
                            ], null);
        }

        if ($form->isSubmitted()) {
            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($newObject);
            }
            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $submittedObject = $form->getData();
                $this->admin->setSubject($submittedObject);
                try {
                    $this->admin->checkAccess('create', $submittedObject);
                } catch (AccessDeniedException $e) {
                    $this->addFlash('sonata_flash_error', $this->trans('errors.404.title', [], 'messages'));

                    return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
                } catch (\Exception $e) {
                    $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

                    return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
                }

                try {
                    $newObject = $this->admin->create($submittedObject);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson([
                                    'result' => 'ok',
                                    'objectId' => $this->admin->getNormalizedIdentifier($newObject),
                                        ], 200, []);
                    }

                    $this->addFlash('sonata_flash_success', $this->trans('flash_create_success', ['%name%' => $this->escapeHtml($this->admin->toString($newObject))], 'SonataAdminBundle'));

                    // redirect to list mode
                    return $this->redirectTo($newObject);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                } catch (\Exception $e) {
                    $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', $this->trans('flash_create_error', ['%name%' => $this->escapeHtml($this->admin->toString($newObject))], 'SonataAdminBundle'));
                }
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $formView = $form->createView();
        // set the theme for the current Admin Form
        $this->setFormTheme($formView, $this->admin->getFormTheme());

        return $this->renderWithExtraParams($this->admin->getTemplate($templateKey), [
                    'action' => 'create',
                    'form' => $formView,
                    'object' => $newObject,
                    'objectId' => null,
                        ], null);
    }

    /**
     * Show action.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response
     */
    public function showAction($id = null) {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        try {
            $this->admin->checkAccess('show', $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $preResponse = $this->preShow($request, $object);
        if (null !== $preResponse) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        return $this->renderWithExtraParams($this->admin->getTemplate('show'), [
                    'action' => 'show',
                    'object' => $object,
                    'elements' => $this->admin->getShow(),
                        ], null);
    }

    /**
     * Show history revisions for object.
     *
     * @param int|string|null $id
     *
     * @throws AccessDeniedException If access is not granted
     * @throws NotFoundHttpException If the object does not exist or the audit reader is not available
     *
     * @return Response
     */
    public function historyAction($id = null) {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        try {
            $this->admin->checkAccess('history', $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $manager = $this->get('sonata.admin.audit.manager');

        if (!$manager->hasReader($this->admin->getClass())) {
            $this->addFlash('sonata_flash_error', $this->trans('no_audit_reader_for_class', ['%class%' => $this->admin->getClass()], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $reader = $manager->getReader($this->admin->getClass());

        $revisions = $reader->findRevisions($this->admin->getClass(), $id);

        return $this->renderWithExtraParams($this->admin->getTemplate('history'), [
                    'action' => 'history',
                    'object' => $object,
                    'revisions' => $revisions,
                    'currentRevision' => $revisions ? current($revisions) : false,
                        ], null);
    }

    /**
     * View history revision of object.
     *
     * @param int|string|null $id
     * @param string|null     $revision
     *
     * @throws AccessDeniedException If access is not granted
     * @throws NotFoundHttpException If the object or revision does not exist or the audit reader is not available
     *
     * @return Response
     */
    public function historyViewRevisionAction($id = null, $revision = null) {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        try {
            $this->admin->checkAccess('historyViewRevision', $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $manager = $this->get('sonata.admin.audit.manager');

        if (!$manager->hasReader($this->admin->getClass())) {
            $this->addFlash('sonata_flash_error', $this->trans('no_audit_reader_for_class', ['%class%' => $this->admin->getClass()], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $reader = $manager->getReader($this->admin->getClass());

        // retrieve the revisioned object
        $object = $reader->find($this->admin->getClass(), $id, $revision);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('no_target_object_for_revision', ['%object%' => $id, '%revision%' => $revision, '%classname%' => $this->admin->getClass()], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $this->admin->setSubject($object);

        return $this->renderWithExtraParams($this->admin->getTemplate('show'), [
                    'action' => 'show',
                    'object' => $object,
                    'elements' => $this->admin->getShow(),
                        ], null);
    }

    /**
     * Compare history revisions of object.
     *
     * @param int|string|null $id
     * @param int|string|null $base_revision
     * @param int|string|null $compare_revision
     *
     * @throws AccessDeniedException If access is not granted
     * @throws NotFoundHttpException If the object or revision does not exist or the audit reader is not available
     *
     * @return Response
     */
    public function historyCompareRevisionsAction($id = null, $base_revision = null, $compare_revision = null) {
        $request = $this->getRequest();

        try {
            $this->admin->checkAccess('historyCompareRevisions');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $manager = $this->get('sonata.admin.audit.manager');

        if (!$manager->hasReader($this->admin->getClass())) {
            $this->addFlash('sonata_flash_error', $this->trans('no_audit_reader_for_class', ['%class%' => $this->admin->getClass()], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $reader = $manager->getReader($this->admin->getClass());

        // retrieve the base revision
        $base_object = $reader->find($this->admin->getClass(), $id, $base_revision);
        if (!$base_object) {
            $this->addFlash('sonata_flash_error', $this->trans('no_target_object_for_revision', ['%object%' => $id, '%revision%' => $base_revision, '%classname%' => $this->admin->getClass()], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        // retrieve the compare revision
        $compare_object = $reader->find($this->admin->getClass(), $id, $compare_revision);
        if (!$compare_object) {
            $this->addFlash('sonata_flash_error', $this->trans('no_target_object_for_revision', ['%object%' => $id, '%revision%' => $compare_object, '%classname%' => $this->admin->getClass()], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        $this->admin->setSubject($base_object);

        return $this->renderWithExtraParams($this->admin->getTemplate('show_compare'), [
                    'action' => 'show',
                    'object' => $base_object,
                    'object_compare' => $compare_object,
                    'elements' => $this->admin->getShow(),
                        ], null);
    }

    /**
     * Export data to specified format.
     *
     * @throws AccessDeniedException If access is not granted
     * @throws \RuntimeException     If the export format is invalid
     *
     * @return Response
     */
    public function exportAction(Request $request) {
        try {
            $this->admin->checkAccess('export');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.title', [], 'messages'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $format = $request->get('format');

        // NEXT_MAJOR: remove the check
        if (!$this->has('sonata.admin.admin_exporter')) {
            @trigger_error('Not registering the exporter bundle is deprecated since version 3.14.' . ' You must register it to be able to use the export action in 4.0.', E_USER_DEPRECATED);
            $allowedExportFormats = (array) $this->admin->getExportFormats();

            $class = $this->admin->getClass();
            $filename = sprintf('export_%s_%s.%s', strtolower(substr($class, strripos($class, '\\') + 1)), date('Y_m_d_H_i_s', strtotime('now')), $format);
            $exporter = $this->get('sonata.admin.exporter');
        } else {
            $adminExporter = $this->get('sonata.admin.admin_exporter');
            $allowedExportFormats = $adminExporter->getAvailableFormats($this->admin);
            $filename = $adminExporter->getExportFilename($this->admin, $format);
            $exporter = $this->get('sonata.exporter.exporter');
        }

        if (!in_array($format, $allowedExportFormats)) {
            $this->addFlash('sonata_flash_error', $this->trans('export_format_not_allowed', ['%format%' => $format, '%class%' => $this->admin->getClass(), '%allowed_formats%' => implode(', ', $allowedExportFormats)], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateUrl('sonata_admin_dashboard'));
        }

        return $exporter->getResponse($format, $filename, $this->admin->getDataSourceIterator());
    }

    /**
     * Returns the Response object associated to the acl action.
     *
     * @param int|string|null $id
     *
     * @throws AccessDeniedException If access is not granted
     * @throws NotFoundHttpException If the object does not exist or the ACL is not enabled
     *
     * @return Response|RedirectResponse
     */
    public function aclAction($id = null) {
        $request = $this->getRequest();

        if (!$this->admin->isAclEnabled()) {
            $this->addFlash('sonata_flash_error', $this->trans('acl_not_enabled', [], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateUrl('sonata_admin_dashboard'));
        }

        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->admin->generateObjectUrl('list', $object));
        }

        try {
            $this->admin->checkAccess('acl', $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->manageUnknownErrors($e));

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $this->admin->setSubject($object);
        $aclUsers = $this->getAclUsers();
        $aclRoles = $this->getAclRoles();

        $adminObjectAclManipulator = $this->get('sonata.admin.object.manipulator.acl.admin');
        $adminObjectAclData = new AdminObjectAclData($this->admin, $object, $aclUsers, $adminObjectAclManipulator->getMaskBuilderClass(), $aclRoles);

        $aclUsersForm = $adminObjectAclManipulator->createAclUsersForm($adminObjectAclData);
        $aclRolesForm = $adminObjectAclManipulator->createAclRolesForm($adminObjectAclData);

        if ('POST' === $request->getMethod()) {
            if ($request->request->has(AdminObjectAclManipulator::ACL_USERS_FORM_NAME)) {
                $form = $aclUsersForm;
                $updateMethod = 'updateAclUsers';
            } elseif ($request->request->has(AdminObjectAclManipulator::ACL_ROLES_FORM_NAME)) {
                $form = $aclRolesForm;
                $updateMethod = 'updateAclRoles';
            }

            if (isset($form)) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $adminObjectAclManipulator->$updateMethod($adminObjectAclData);
                    $this->addFlash('sonata_flash_success', $this->trans('flash_acl_edit_success', [], 'SonataAdminBundle'));

                    return new RedirectResponse($this->admin->generateObjectUrl('acl', $object));
                }
            }
        }

        return $this->renderWithExtraParams($this->admin->getTemplate('acl'), [
                    'action' => 'acl',
                    'permissions' => $adminObjectAclData->getUserPermissions(),
                    'object' => $object,
                    'users' => $aclUsers,
                    'roles' => $aclRoles,
                    'aclUsersForm' => $aclUsersForm->createView(),
                    'aclRolesForm' => $aclRolesForm->createView(),
                        ], null);
    }

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Si estamos en el entorno de dev y somos SuperAdmin vamos a mostrar un mensaje mas descriptivo
     * 
     * @param \Exception $e
     */
    protected function manageUnknownErrors($e) {
        $isDev = ('dev' === $this->container->get('kernel')->getEnvironment());
        $isSuperAdmin = ($this->admin->getCurrentUser()->hasRolePrincipal('ROLE_SUPER_ADMIN'));

        $fullMessage = $e->getMessage() . ' (' . $e->getFile() . '::' . $e->getLine() . ')';

        return ($isDev && $isSuperAdmin) ? $fullMessage : $this->trans('unknown_error', [], 'SonataAdminBundle');
    }

    /**
     * Ponemos por defecto todos los datos de la llamada de error
     * 
     * @param array|null $data Datos para devolver
     */
    protected function setDefaultSuccessResponse($message = null, $data = null) {
        $this->response = [
            'success' => true,
            'message' => $message
        ];

        if (null !== $data) {
            foreach ($data as $key => $value) {
                $this->response['data'][$key] = $value;
            }
        }

        $this->response['status'] = 200;
    }

    /**
     * Ponemos por defecto todos los datos de la llamada de error
     * 
     * @param \Exception  $e             Excepcionn lanzada
     * @param string|null $customMessage Mensaje personalizado
     */
    protected function setDefaultErrorResponse($message = null, $data = null, $status = 200) {
        $this->response = [
            'success' => false,
            'message' => $message
        ];

        if (null !== $data) {
            foreach ($data as $key => $value) {
                $this->response['data'][$key] = $value;
            }
        }

        $this->response['status'] = $status;
    }

    /**
     * Ponemos por defecto todos los datos de la llamada de params missing
     */
    protected function setDefaultParamsMissingResponse() {
        $this->response = [
            'success' => false,
            'message' => 'SOME PARAMETERS ARE MISSING',
            'data' => null,
            'status' => 428,
        ];
    }

    /**
     * Traduce un texto desde los archivos de traduccion en un idioma en concreto
     * 
     * @param Request $request
     * @param string  $message
     * @param array   $params
     * 
     * @return string
     */
    protected function translate($request, $message, $params = [], $translationDomain = null) {
        if (!$translationDomain) {
            $translationDomain = $this->translationDomain;
        }

        $lang = $request->request->get('lang');
        $lang = ($lang) ?: 'es';

        return $this->get('translator')->trans($message, $params, $translationDomain, $lang);
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the admin form theme to form view. Used for compatibility between Symfony versions.
     *
     * @param string $theme
     */
    private function setFormTheme(FormView $formView, $theme) {
        $twig = $this->get('twig');

        // BC for Symfony < 3.2 where this runtime does not exists
        if (!method_exists(AppVariable::class, 'getToken')) {
            $twig->getExtension(FormExtension::class)->renderer->setTheme($formView, $theme);

            return;
        }

        // BC for Symfony < 3.4 where runtime should be TwigRenderer
        if (!method_exists(DebugCommand::class, 'getLoaderPaths')) {
            $twig->getRuntime(TwigRenderer::class)->setTheme($formView, $theme);

            return;
        }

        $twig->getRuntime(FormRenderer::class)->setTheme($formView, $theme);
    }

}
