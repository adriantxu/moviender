<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AbstractController extends FOSRestController {

    protected $responseData = [
        'success' => false,
        'message' => 'NO DIRECT ACCESS ALLOWED',
        'data' => null,
    ];
    protected $responseStatus = 401;

    /**
     * Comprueba si viene el token en los headers y lo devuelve
     * 
     * @param Request $request
     * 
     * @return string
     */
    protected function getToken(Request $request) {
        foreach ([
    'token',
        ] as $requiredParam) {
            if (!($$requiredParam = $request->headers->get($requiredParam))) {
                $this->setDefaultErrorResponse($this->setDefaultErrorResponse($this->translate('Api', 'errors.mandatory_param', ['%PARAMETRO%' => $requiredParam])));

                return null;
            }
        }

        return $token;
    }

    /**
     * Comprueba y devuelve un usuario si existe por su token
     * 
     * @param string $token
     * 
     * @return null|\Application\Sonata\UserBundle\Entity\User
     */
    protected function getUserByToken($token) {
        $result = null;

        if (!$token) {
            $this->setErrorResponse($this->translate('Api', 'errors.mandatory_param', ['%PARAMETRO%' => 'token']));
        } else {
            try {
                $user = $this->get('fos_user.user_manager')->findUserBy(['token' => $token]);

                // Si no existe el usuario sacamos un error
                if (!$user instanceof \Application\Sonata\UserBundle\Entity\User) {
                    throw new \Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
                }

                // Comprobamos que el usuario este activo
                if (true !== $user->isEnabled()) {
                    throw new \Symfony\Component\Security\Core\Exception\DisabledException();
                }

                $result = $user;
            } catch (\Symfony\Component\Security\Core\Exception\UsernameNotFoundException $e) {
                $this->setDefaultErrorResponse($this->translate('Api', 'errors.login.not_found_user'));
            } catch (\Symfony\Component\Security\Core\Exception\DisabledException $e) {
                $this->setDefaultErrorResponse($this->translate('Api', 'errors.login.account_disabled'));
            } catch (\Exception $e) {
                $this->setDefaultErrorResponse($this->translate('SonataAdminBundle', 'unknown_error', [], true, 'xliff'));
            }
        }

        return $result;
    }

    /**
     * Ponemos por defecto todos los datos de la llamada de error
     * 
     * @param string|null $message Mensaje personalizado
     * @param array|null  $data    Campos extra
     * @param integer     $status  Status de la llamada
     */
    protected function setDefaultErrorResponse($message = null, $data = null, $status = 200) {
        $this->responseData = [
            'success' => false,
            'message' => $message
        ];

        if (null !== $data) {
            foreach ($data as $key => $value) {
                $this->responseData['data'][$key] = $value;
            }
        }

        $this->responseStatus = $status;
    }

    /**
     * Ponemos por defecto todos los datos de la llamada de error
     * 
     * @param string|null $message Mensaje personalizado
     * @param array|null  $data    Campos extra
     */
    protected function setDefaultSuccessResponse($message = null, $data = null) {
        $this->responseData = [
            'success' => true,
            'message' => $message
        ];

        if (null !== $data) {
            foreach ($data as $key => $value) {
                $this->responseData['data'][$key] = $value;
            }
        }

        $this->responseStatus = 200;
    }

    /**
     * Devuelve un mensaje traduc
     * 
     * @param string  $domain     Domain del texto a traducir
     * @param string  $message    Mensaje a traducir
     * @param array   $parameters Listado de parametros
     * @param boolean $isFromApp  Flag para saber si hay que buscar dentro del directorio app o no
     * @param string  $extension  Extension del archivo de traduccion
     * 
     * @return string
     */
    protected function translate($domain, $message, $parameters = [], $isFromApp = false, $extension = 'yml') {
        $translator = \AppBundle\DependencyInjection\UtilsExtension::getTranslator($domain, $isFromApp, $extension);

        return $translator->trans($message, $parameters);
    }

}
