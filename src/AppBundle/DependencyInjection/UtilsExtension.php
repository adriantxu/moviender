<?php

namespace AppBundle\DependencyInjection;

class UtilsExtension {

    /**
     * Devuelve un listado de idiomas de la aplicacion
     * 
     * @return array
     */
    public static function getLanguages() {
        return [
            'es' => 'Castellano',
            'eu' => 'Euskera',
        ];
    }

    /**
     * Devuelve un texto que hay entre un texto de inicio y otro final
     * 
     * @param string $string
     * @param string $start
     * @param string $end
     * 
     * @return string
     */
    public static function getStrBetween($string, $start, $end) {
        $string = ' ' . $string;

        $ini = strpos($string, $start);

        if ($ini == 0) {
            return null;
        }

        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }

    /**
     * Devuelve el translator para poder traducir
     * Es obligatorio que el directorio y el archivo se llamen igual
     * 
     * @param str|null $domain    Opcional. Archivo de donde tiene que sacar las tracucciones
     * @param bool     $isFromApp Si viene a true hay que cargar el archivo de traducciones desde el directorio app
     * @param str      $extension Extension del fichero. Por defecto yml
     * 
     * @return \Symfony\Component\Translation\Translator
     */
    public static function getTranslator($domain = null, $isFromApp = false, $extension = 'yml') {
        $resource = ('yml' === $extension) ? 'yaml' : $extension;

        if (!$domain) {
            $domain = str_replace('Entity', '', substr(strrchr(debug_backtrace()[2]['class'], "\\"), 1));
        }

        $locale = (isset($_SESSION['_sf2_attributes']) && isset($_SESSION['_sf2_attributes']['_locale'])) ? $_SESSION['_sf2_attributes']['_locale'] : 'es';

        $translator = new \Symfony\Component\Translation\Translator($locale);

        $loader = '\Symfony\Component\Translation\Loader\\' . ucfirst($resource) . 'FileLoader';
        $translator->addLoader($resource, new $loader());

        if ($isFromApp) {
            $fileUrl = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . $domain . DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR . $domain . '.' . $locale . '.' . $extension;
        } else {
            $fileUrl = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR . lcfirst($domain) . DIRECTORY_SEPARATOR . $domain . '.' . $locale . '.' . $extension;
        }

        $translator->addResource($resource, $fileUrl, $locale);

        return $translator;
    }

    /**
     * Devuelve un campo booleano traducido
     * 
     * @param int|boolean $value
     * 
     * @return string
     */
    final public static function getBooleanTrans($value) {
        $translator = self::getTranslator('SonataAdminBundle', true, 'xliff');

        if ($value) {
            return $translator->trans('label_type_yes');
        } else {
            return $translator->trans('label_type_no');
        }
    }

}
