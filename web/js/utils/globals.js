/**
 * Muestra un mensaje de error para un campo en concreto
 * 
 * @param {DOM}     field       Campo al que hacer referencia
 * @param {boolean} show        Flag para saber si hay que esconder el mensaje o no
 * @param {string}  message     Mensaje para mostrar
 * 
 * @returns {undefined}
 */
function triggerSonataError(field, show, message) {
    if (show) {
        $(field).closest('.form-group').addClass('has-error');
        $(field).closest('.sonata-ba-field').addClass('sonata-ba-field-error');
        $(field).closest('.sonata-ba-field').append(
                '<div class="help-block sonata-ba-field-error-messages"> \
                    <ul class="list-unstyled"> \
                        <li><i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' + ((message) ? message + '.' : '') + '</li> \
                    </ul> \
                </div>'
                );
    } else {
        $(field).closest('.form-group').removeClass('has-error');
        $(field).closest('.sonata-ba-field').removeClass('sonata-ba-field-error');
        $(field).closest('.sonata-ba-field').find('.help-block').remove();
    }
}
;

/**
 * Comprobamos si estamos en el entorno de desarrollo
 * 
 * @returns {boolean}
 */
function isDev() {
    return window.location.href.indexOf('/app_dev/') > -1;
}
;