imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: es
    locales: ['es']
    season_timeout_seconds: 900

framework:
    #esi: ~
    translator: { fallbacks: '%locales%' }
    secret: '%secret%'
    router:
        resource: '%kernel.project_dir%/app/config/routing.yml'
        strict_requirements: ~
    form: ~
    csrf_protection: ~
    validation: { enable_annotations: true }
    #serializer: { enable_annotations: true }
    default_locale: '%locale%'
    trusted_hosts: ~
    session:
        # https://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id: session.handler.native_file
        save_path: '%kernel.project_dir%/var/sessions/%kernel.environment%'
    fragments: ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true
    templating:
        engines: ['twig']

# Twig Configuration
twig:
    debug: '%kernel.debug%'
    form_themes:
        - 'Form/free_html.html.twig'
        - 'SonataCoreBundle:Form:datepicker.html.twig'
        - ::form_div_layout.html.twig
    strict_variables: '%kernel.debug%'
    globals:
        utils: '@app.utils'

# Doctrine Configuration
doctrine:
    dbal:
        driver: pdo_mysql
        host: '%database_host%'
        port: '%database_port%'
        dbname: '%database_name%'
        user: '%database_user%'
        password: '%database_password%'
        charset: UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: '%kernel.project_dir%/var/data/data.sqlite'
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #path: '%database_path%'
        types:
            json: Sonata\Doctrine\Types\JsonType

    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: '%mailer_transport%'
    host: '%mailer_host%'
    username: '%mailer_user%'
    password: '%mailer_password%'
    spool: { type: memory }

sonata_admin:
    title: 'Moviender'
    title_logo: img/logo-icon.png

    options:
        title_mode: both
        confirm_exit: false

    show_mosaic_button: false

    assets:
        extra_stylesheets:
            - vendor/eonasdan-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css
            - css/styles.css
        remove_stylesheets:
            - bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css

        extra_javascripts:
            - bundles/sonatacore/vendor/bootstrap/js/transition.js
            - bundles/sonatacore/vendor/bootstrap/js/collapse.js
            - vendor/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.js
            - js/utils.js
        remove_javascripts:
            - bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js

    security:
        handler: sonata.admin.security.handler.role

        role_admin: ROLE_ADMIN
        role_super_admin: ROLE_SUPER_ADMIN

sonata_block:
    blocks:
        # enable the SonataAdminBundle block
        sonata.admin.block.admin_list:
            contexts: [admin]
        # enable the search function
        sonata.admin.block.search_result:
            contexts: [admin]

sonata_user:
    security_acl: true
    manager_type: orm                                                           # can be orm or mongodb
    class:
        user: Application\Sonata\UserBundle\Entity\User
        group: Application\Sonata\UserBundle\Entity\Group
    impersonating:
        route: sonata_admin_dashboard

fos_user:
    db_driver:      orm                                                         # can be orm or odm
    firewall_name:  main
    user_class:     Application\Sonata\UserBundle\Entity\User

    group:
        group_class:   Application\Sonata\UserBundle\Entity\Group
        group_manager: sonata.user.orm.group_manager                            # If you're using doctrine orm (use sonata.user.mongodb.group_manager for mongodb)

    service:
        user_manager: sonata.user.orm.user_manager

    from_email:
        address: '%mailer_user%'
        sender_name: '%mailer_user%'

monolog:
    handlers:
        main:
            type: stream
            path: "%kernel.logs_dir%/%kernel.environment%.log"
            level: debug
            formatter: monolog.formatter.standard