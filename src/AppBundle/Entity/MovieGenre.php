<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MovieGenre
 *
 * @ORM\Table(name="movie_genre")
 * @ORM\Entity(repositoryClass="\AppBundle\Repository\MovieGenreRepository")
 */
class MovieGenre extends \AppBundle\Entity\AbstractEntity {

    /**
     * Identificador del genero en TheMovieDB
     * 
     * @var integer
     *
     * @ORM\Column(name="genre_id", type="integer", unique=true)
     */
    private $genreId;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", nullable=true)
     */
    private $thumbnail;

    /**
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Movie", inversedBy="genres", cascade={"persist"})
     */
    private $movies;

    public function __construct() {
        parent::__construct();

        $this->movies = new ArrayCollection();
    }

    public function getGenreId() {
        return $this->genreId;
    }

    public function setGenreId($genreId) {
        $this->genreId = $genreId;
    }

    public function getThumbnail() {
        return $this->thumbnail;
    }

    public function setThumbnail($thumbnail) {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getMovies() {
        return $this->movies;
    }

    public function addMovie(\AppBundle\Entity\Movie $movie = null) {
        if ($this->movies->contains($movie)) {
            return null;
        }

        $this->movies[] = $movie;

        $movie->addGenre($this);

        return $this;
    }

    public function removeMovie(\AppBundle\Entity\Movie $movie = null) {
        if (!$this->movies->contains($movie)) {
            return null;
        }

        $this->movies->removeElement($movie);

        $movie->removeGenre($this);

        return $this;
    }

}
