<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin as BaseAbstractAdmin;
use Sonata\AdminBundle\Form\Type\Filter\DateTimeType;
use Sonata\AdminBundle\Route\RouteCollection;

class AbstractAdmin extends BaseAbstractAdmin {

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName) {
        parent::__construct($code, $class, $baseControllerName);

        // Calculamos el translation_domain
        $classReflection = new \ReflectionClass($this);

        $this->setTranslationDomain(str_replace('Admin', '', $classReflection->getShortName()));
    }

    /**
     * @final Este metodo es necesario para cargar los JS y CSS en las plantillas twig, no hay que modificarlo, ni heredarlo ni hacer nada mas con el
     * 
     * Devolvemos la propiedad formOptions
     * 
     * @return array
     */
    final public function getFormOptions() {
        return $this->formOptions;
    }

    /**
     * Devuelve el usuario logueado
     * Si no tiene token redirigimos al dashboard
     * 
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    final public function getCurrentUser() {
        if ($token = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()) {
            return $token->getUser();
        } else {
            $redirection = new RedirectResponse($this->routeGenerator->generate('sonata_admin_dashboard'));
            $redirection->send();

            die;
        }
    }

    /**
     * No dejamos heredar el metodo para que no se salte la comprobacion del metodo preventDelete
     * 
     * @param \Application\Sonata\UserBundle\Entity\User $object
     */
    final public function preRemove($object) {
        if (true === $this->preventDelete($object->getId())) {
            $redirection = new \Symfony\Component\HttpFoundation\RedirectResponse($this->generateUrl('list'));
            $redirection->send();

            die;
        }
    }

    /**
     * No dejamos heredar el metodo para que no se salte la comprobacion del metodo preventDelete
     * 
     * @param str                                              $actionName
     * @param \Sonata\AdminBundle\Datagrid\ProxyQueryInterface $query
     * @param array                                            $idx
     * @param boolean                                          $allElements
     */
    final public function preBatchAction($actionName, \Sonata\AdminBundle\Datagrid\ProxyQueryInterface $query, array &$idx, $allElements) {
        if ('delete' === $actionName) {
            $ids = $idx;

            if ($allElements && empty($ids)) {
                $items = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository($this->getClass())->findAll();

                $ids = array_map(create_function('$e', 'return $e->getId();'), $items);
            }

            foreach ($ids as $id) {
                if (true === $this->preventDelete($id)) {
                    $redirection = new \Symfony\Component\HttpFoundation\RedirectResponse($this->generateUrl('list'));
                    $redirection->send();

                    die;
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    protected function configureRoutes(RouteCollection $collection) {
        $collection->remove('show');
    }

    /**
     * Comprobamos si el objeto es nuevo o ya existe
     * 
     * @return boolean
     */
    final protected function isNew() {
        return (null === $this->getSubject()) ? true : ((null === $this->getSubject()->getId()) ? true : false);
    }

    /**
     * Por defecto devolvemos false para que se pueda eliminar el objeto
     * 
     * Las clases que necesiten hacer una comprobacion tendran que sobrecargar este metodo
     * 
     * @param int $id
     */
    protected function preventDelete($id) {
        return false;

        $elemento = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository($this->getClass())->find($id);
    }

    /**
     * Metodo para filtrar los campos de fecha que son de tipo doctrine_orm_callback
     * 
     * Se utiliza este metodo cuando se quiere buscar por Y-m-d pero en la base de datos se ha guardado como Y-m-d H:i:s
     * 
     * @param \Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery $queryBuilder
     * @param string                                             $alias
     * @param string                                             $field
     * @param array                                              $value
     * 
     * @return \Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery
     */
    protected function processDateFilters($queryBuilder, $alias, $field, $value) {
        if ((array_key_exists('type', $value)) && (array_key_exists('type', $value['type']))) {
            switch ($value['type']['type']) {
                // Si no tiene valor o el valor es TYPE_EQUAL buscamos por el año-mes-dia sin tener en cuenta las horas, minutos, segundos
                case null:
                case DateTimeType::TYPE_EQUAL:
                    $queryBuilder
                            ->andWhere($alias . '.' . $field . ' BETWEEN :' . strtoupper($field) . '_FECHA_INICIO AND :' . strtoupper($field) . '_FECHA_FIN')
                            ->setParameter(strtoupper($field) . '_FECHA_INICIO', $value['value']->format('Y-m-d 00:00:00'))
                            ->setParameter(strtoupper($field) . '_FECHA_FIN', $value['value']->format('Y-m-d 23:59:59'))
                    ;

                    break;

                case DateTimeType::TYPE_GREATER_EQUAL:
                    $queryBuilder
                            ->andWhere($alias . '.' . $field . ' >= :' . strtoupper($field) . '_DATE')->setParameter(strtoupper($field) . '_DATE', $value['value']->format('Y-m-d H:i:s'))
                    ;

                    break;
                case DateTimeType::TYPE_GREATER_THAN:
                    $queryBuilder
                            ->andWhere($alias . '.' . $field . ' > :' . strtoupper($field) . '_DATE')->setParameter(strtoupper($field) . '_DATE', $value['value']->format('Y-m-d H:i:s'))
                    ;

                    break;
                case DateTimeType::TYPE_LESS_EQUAL:
                    $queryBuilder
                            ->andWhere($alias . '.' . $field . ' <= :' . strtoupper($field) . '_DATE')->setParameter(strtoupper($field) . '_DATE', $value['value']->format('Y-m-d H:i:s'))
                    ;

                    break;
                case DateTimeType::TYPE_LESS_THAN:
                    $queryBuilder
                            ->andWhere($alias . '.' . $field . ' < :' . strtoupper($field) . '_DATE')->setParameter(strtoupper($field) . '_DATE', $value['value']->format('Y-m-d H:i:s'))
                    ;

                    break;
                case DateTimeType::TYPE_NULL:
                    $queryBuilder
                            ->andWhere($alias . '.' . $field . ' IS NULL')
                    ;

                    break;
                case DateTimeType::TYPE_NOT_NULL:
                    $queryBuilder
                            ->andWhere($alias . '.' . $field . ' IS NOT NULL')
                    ;

                    break;
            }
        }

        return $queryBuilder;
    }

}
