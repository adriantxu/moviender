<?php

namespace Application\Sonata\AdminBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as BaseCRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CRUDController extends BaseCRUDController {

    /**
     * List action.
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response
     */
    public function listAction() {
        try {
            $this->admin->checkAccess('list');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.message', [], 'errors'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->trans('unknown_error', [], 'SonataAdminBundle'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        }

        return parent::listAction();
    }

    /**
     * {@inheritdoc}
     */
    public function createAction() {
        try {
            $this->admin->checkAccess('create');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.message', [], 'errors'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->trans('unknown_error', [], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateUrl('list'));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }

        $action = parent::createAction();

        // No redirigimos al listado si el usuario logueado no tiene permisos para acceder al listado
        if (method_exists($action, 'getTargetUrl') && $this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
            return $this->redirectToList();
        }

        return $action;
    }

    /**
     * {@inheritdoc}
     */
    public function editAction($id = null) {
        $request = $this->getRequest();

        $id = $request->get($this->admin->getIdParameter());
        $existingObject = $this->admin->getObject($id);

        if (!$existingObject) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateObjectUrl('list', $existingObject));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }

        try {
            $this->admin->checkAccess('edit', $existingObject);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.message', [], 'errors'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->trans('unknown_error', [], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateObjectUrl('list', $existingObject));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }

        $action = parent::editAction($id);

        // No redirigimos al listado si el usuario logueado no tiene permisos para acceder al listado
        if (method_exists($action, 'getTargetUrl') && $this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
            return $this->redirectToList();
        }

        return $action;
    }

    /**
     * Show action.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response
     */
    public function showAction($id = null) {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateObjectUrl('list', $object));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }

        try {
            $this->admin->checkAccess('show', $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.message', [], 'errors'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->trans('unknown_error', [], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateObjectUrl('list', $object));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }


        return parent::showAction($id);
    }

    /**
     * Execute a batch delete.
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return RedirectResponse
     */
    public function batchActionDelete(ProxyQueryInterface $query) {
        try {
            $this->admin->checkAccess('batchDelete');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.message', [], 'errors'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->trans('unknown_error', [], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateObjectUrl('list', $this->admin));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }

        return parent::batchActionDelete($query);
    }

    /**
     * Delete action.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function deleteAction($id) {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans('object_with_id_not_found', ['%id%' => $id], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateObjectUrl('list', $object));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }

        try {
            $this->admin->checkAccess('delete', $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', $this->trans('errors.404.message', [], 'errors'));

            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->trans('unknown_error', [], 'SonataAdminBundle'));

            if ($this->admin->isGranted('LIST', $this->admin->getCurrentUser())) {
                return $this->redirect($this->admin->generateObjectUrl('list', $object));
            } else {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
        }

        return parent::deleteAction($id);
    }

}
