<?php

namespace ApiBundle\Controller;

use ApiBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("movie/", name="api_movie")
 */
class MovieController extends AbstractController {

    /**
     * Devuelve todas las Movie por su MovieGenre
     * 
     * @Route("all/genre", name="api_movie_all_genre", methods={"POST"})
     */
    public function getAllMoviesByGenreAction(Request $request) {
        if ($token = $this->getToken($request)) {
            if ($user = $this->getUserByToken($token)) {
                // Parametros obligatorios
                foreach ([
            'genre',
                ] as $requiredParam) {
                    if (!($$requiredParam = $request->request->get($requiredParam))) {
                        $this->setDefaultErrorResponse($this->translate('Api', 'errors.mandatory_param', ['%PARAMETRO%' => $requiredParam]));

                        return new JsonResponse($this->responseData, $this->responseStatus);
                    }
                }

                // Parametros opcionales
                foreach ([
            'page',
                ] as $requiredParam) {
                    $$requiredParam = $request->request->get($requiredParam);
                }

                if (!$page) {
                    $page = 1;
                }

                $data = [];

                $items = \AppBundle\DependencyInjection\TheMovieDBExtension::callUrl('discover/movie', ['with_genres' => $genre, 'page' => $page]);

                foreach ($items['results'] as $item) {
                    $data[] = [
                        'id' => $item['id'],
                        'popularity' => $item['popularity'],
                        'vote_count' => $item['vote_count'],
                        'poster_path' => \AppBundle\DependencyInjection\TheMovieDBExtension::getPosterPath($item['poster_path']),
                        'original_language' => $item['original_language'],
                        'genre_ids' => $item['genre_ids'],
                        'title' => $item['title'],
                        'vote_average' => $item['vote_average'],
                        'overview' => $item['overview'],
                        'release_date' => $item['release_date'],
                    ];
                }

                $this->setDefaultSuccessResponse(null, $data);
            }
        }

        return new JsonResponse($this->responseData, $this->responseStatus);
    }

}
