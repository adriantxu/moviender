<?php

namespace AppBundle\Entity;

use AppBundle\DependencyInjection\UtilsExtension;
use Doctrine\ORM\Mapping as ORM;

/**
 * @todo Revisar los lifecycle callbacks para poder sacar el em
 * 
 * @see https://symfony.com/doc/current/doctrine/events.html#doctrine-lifecycle-callbacks
 */
class AbstractEntity {

    protected $dbname;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Como el nombre va a ir en varias Entity lo agregamos aqui
     * 
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    public function __construct() {
        
    }

    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __toString() {
        if (method_exists($this, 'getName') && null !== $this->name) {
            return $this->getName();
        } else {
            return $this->getClassName();
        }
    }

    public function setObjectVars(array $vars) {
        foreach ($vars as $name => $oldValue) {
            $method = 'set' . ucfirst($name);

            if (method_exists($this, $method)) {
                $this->$method(isset($vars[$name]) ? $vars[$name] : null);
            }
        }
    }

    protected function getClassName() {
        $path = explode('\\', get_class($this));

        return array_pop($path);
    }

    /**
     * Devuelve un objeto en array
     * 
     * @param array $extraFields Opcional. Campos extra que se devuelven para cada clase
     * 
     * @return array
     */
    public function toArray($extraFields = []) {
        $extraFields = array_change_key_case($extraFields, CASE_LOWER);

        $a = [];

        $reflection = new \ReflectionClass(get_class($this));
        foreach ($reflection->getProperties() as $reflectionProperty) {
            if ($reflection->getName() === $reflectionProperty->class) {

                $method = 'get' . ucfirst($reflectionProperty->name);

                if (method_exists($this, $method)) {
                    $property = $this->$method();

                    // DateTime
                    if ($property && $property instanceof \DateTime) {
                        $property = $property->format('Y-m-d H:i:s');
                    }

                    // Entity
                    if ($property && (stristr($reflectionProperty->getDocComment(), '@ORM\ManyToOne') || (stristr($reflectionProperty->getDocComment(), '@ORM\OneToOne')))) {
                        $property = array_merge(['id' => $property->getId()], $property->getNameFields());
                    }

                    // ArrayCollection
                    if ($property && is_object($property) && in_array(get_class($property), ['Doctrine\ORM\PersistentCollection', 'Doctrine\Common\Collections\ArrayCollection'])) {
                        $items = $property;

                        $property = [];

                        foreach ($items as $item) {
                            $fields = ['id' => $item->getId()];

                            $className = $item->getClassName();

                            if (array_key_exists(strtolower($className), $extraFields)) {
                                foreach ($extraFields[strtolower($className)] as $extraProperty) {
                                    $itemReflection = new \ReflectionClass(get_class($item));

                                    foreach ($itemReflection->getProperties() as $p) {
                                        if (strtolower($extraProperty) === strtolower($p->name)) {
                                            $pGetMethod = 'get' . ucfirst($p->name);

                                            $fields[$p->name] = $item->$pGetMethod();

                                            break;
                                        }
                                    }
                                }
                            }

                            $property[] = array_merge($fields, $item->getNameFields());
                        }
                    }

                    $a[$reflectionProperty->name] = $property;
                }
            }
        }

        if (!array_key_exists('id', $a)) {
            $a['id'] = $this->getId();
        }

        return $a;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Subida de ficheros
     * 
     * Para que funcione la ruta tiene que estar en una constante que se llame: NOMBREDELCAMPO_PATH
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param string|array                $fields
     */
    final public function uploadFile(\Doctrine\ORM\EntityManager $em, $fields) {
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        $this->setDbname();

        $reflectionClass = new \ReflectionClass($this);

        foreach ($fields as $field) {
            $propertyName = ucfirst(strtolower($field));

            $getMethod = 'get' . $propertyName . 'File';
            $setMethod = 'set' . $propertyName;
            $constPath = strtoupper($field) . '_PATH';
            $newPath = constant(get_class($this) . '::' . $constPath);

            // El nuevo path tiene que empezar por /
            if ('/' !== substr($newPath, 0, strlen('/'))) {
                $newPath = '/' . $newPath;
            }

            // El nuevo path tiene que terminar por /
            if ('/' !== substr($newPath, -strlen('/'))) {
                $newPath = $newPath . '/';
            }

            // Recogemos el nombre de la columna que esta en el atributo name de la Propertyde la Entity
            $property = $reflectionClass->getProperty($field);
            $columnName = UtilsExtension::getStrBetween($property->getDocComment(), 'name="', '"');

            if ($this->$getMethod()) {
                $sql = 'SELECT
                            ' . $columnName . '
                        FROM
                            ' . $this->dbname . '
                        WHERE
                            id = ?;'
                ;

                $result = $em->getConnection()->fetchAssoc($sql, [$this->id]);

                // Recogemos el nuevo nombre del fichero
                $getMethod = 'get' . $propertyName . 'File';

                $fileName = $this->$getMethod()->getClientOriginalName();
                $extension = pathinfo($fileName, PATHINFO_EXTENSION);

                $isName = true;

                do {
                    $newName = md5(uniqid()) . '.' . $extension;

                    $sql = 'SELECT
                                ' . $columnName . '
                            FROM
                                ' . $this->dbname . '
                            WHERE
                                ' . $columnName . ' = ?;'
                    ;

                    $result = $em->getConnection()->fetchAssoc($sql, [$newName]);

                    $isName = (false !== $result);
                } while ($isName);

                $file = $this->$getMethod();

                $file->move($this->getDocumentRoot() . $newPath, $newName);

                // Comprobamos que la imagen se haya subido correctamente
                if (!is_file($this->getDocumentRoot() . $newPath . $newName)) {
                    throw new \Exception('Image not uploaded');
                }

                // Hay que comprobar si se guardan varios ficheros o un unico fichero
                $class = new \ReflectionClass($this);
                $properties = $class->getProperties();

                foreach ($properties as $p) {
                    if (strtolower($p->name) === strtolower($propertyName)) {
                        $property = $p;

                        break;
                    }
                }

                $docComment = ' ' . $property->getDocComment();

                if (false === strpos($docComment, 'type="array"')) {
                    $this->$setMethod($newPath . $newName);
                } else {
                    $newItem = [$newPath . $newName];

                    $pGetMethod = 'get' . $p->name;

                    $this->$setMethod(array_merge($this->$pGetMethod(), $newItem));
                }
            }
        }
    }

    /**
     * Eliminacion de uno o varios ficheros a una ruta en concreto
     * 
     * @param string|array $fields Campos para los que se suben las imagenes
     */
    final public function removeFile($fields) {
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        foreach ($fields as $field) {
            $getMethod = 'get' . ucfirst(strtolower($field));
            $setMethod = 'set' . ucfirst(strtolower($field));

            if (method_exists($this, $getMethod)) {
                $files = $this->$getMethod();

                if (!is_array($files)) {
                    $files = [$files];
                }

                foreach ($files as $file) {
                    if (!empty($file) && is_file($_SERVER['DOCUMENT_ROOT'] . $file)) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . $file);
                    }
                }

                // Cuando se eliminan todos los ficheros actualizamos el campo y lo ponemos vacio
                if (is_array($files)) {
                    $this->$setMethod([]);
                } else {
                    $this->$setMethod('');
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Sacamos el nombre de la tabla
     */
    protected function setDbname() {
        $class = new \ReflectionClass($this);

        // Eliminamos los saltos de linea, espacios y el asterisco que pudiera haber si la documentacion estuviera en varias lineas
        $docComment = ' ' . str_replace(['\r', '\n', ' '], '', $class->getDocComment());
        $docComment = str_replace('Table(*name=', 'Table(name=', $docComment);

        $this->dbname = UtilsExtension::getStrBetween($docComment, '@ORM\Table(name="', '"');
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Devuelve la ruta hacia la carpeta web
     * 
     * @return string
     */
    private function getDocumentRoot() {
        return __DIR__ . '/../../../web';
    }

}
