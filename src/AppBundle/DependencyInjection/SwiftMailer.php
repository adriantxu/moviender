<?php

namespace AppBundle\DependencyInjection;

class SwiftMailer {

    private $container;

    public function __construct($container) {
        $this->container = $container;
    }

    /**
     * Envia un correo
     * 
     * @param string            $subject     Asunto del correo
     * @param string            $body        Texto del correo
     * @param string|array|null $to          Primary recipients
     * @param string|array|null $cc          Reciben una copia
     * @param string|array|null $bcc         Receptores invisibles a ojos de los demas
     * @param array             $attachments Archivos adjuntos
     * 
     * @return int          0 => false, 1 => true
     */
    public function send($subject, $body, $to = null, $cc = null, $bcc = null, $attachments = []) {
        if (!$to || empty($to)) {
            $defaultEmails = [
                'tankerai@gmail.com',
                    ]
            ;

            $to = $defaultEmails;
        }

        if (!empty($cc)) {
            if (!is_array($cc)) {
                $cc = [$cc];
            }
        }

        if (!empty($bcc)) {
            if (!is_array($bcc)) {
                $bcc = [$bcc];
            }
        }

        $bgEmail = explode('/web/', $_SERVER['HTTP_REFERER'])[0] . '/web/img/bg-email.jpg';

        $message = (new \Swift_Message($subject))
                ->setFrom($this->container->getParameter('mailer_user'))
                ->setTo($to)
                ->setCc($cc)
                ->setBcc($bcc)
                ->setBody(
                        $this->container->get('templating')->render(
                                'Email/html/base.html.twig', [
                            'bgEmail' => $bgEmail,
                            'subject' => $subject,
                            'body' => $body,
                                ]
                        ), 'text/html'
                )
                // If you also want to include a plaintext version of the message
                ->addPart(
                $this->container->get('templating')->render(
                        'Email/txt/base.txt.twig', [
                    'subject' => $subject,
                    'body' => $body,
                        ]
                ), 'text/plain'
                )
        ;

        foreach ($attachments as $attachment) {
            $message->attach(\Swift_Attachment::fromPath($attachment));
        }

        return $this->container->get('mailer')->send($message);
    }

}
