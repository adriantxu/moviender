<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FreeHtmlType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function getParent() {
        return 'Symfony\Component\Form\Extension\Core\Type\TextType';
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'mapped' => false,
            'required' => false,
            'template' => '',
        ]);
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        foreach ([
    'mapped',
    'required',
    'template',
        ] as $buildOption) {
            $view->vars[$buildOption] = $options[$buildOption];
        }
    }

}
